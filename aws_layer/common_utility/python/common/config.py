import os
import configparser
import pathlib

from common.cxlLogger import getLogger

logger = getLogger(__name__)

config = configparser.ConfigParser()

ENV_LIST = ['dev','int','stg']

LAKE_CONFIG = 'lake_config'
SFCC_CONFIG = 'sfcc_config'
AWS_CONFIG = 'aws_config'
COMMON_CONFIG = 'common_config'
DIVA_CONFIG = 'diva_config'
DIVA_MAPPING = 'diva_mapping'
SFCC_ATTRIBUTE_MAPPING = 'sfcc_attribute_mapping'
PRODUCTSUP_CONFIG = 'productsup_config'
PRODUCTSUP_MAPPING = 'productsup_mapping'

def getFileDirPath(_file):
    return str(pathlib.Path(_file).parent.absolute())

# Methods of this file will not work without invoking inialize method atleast once
def initialize():
    config.optionxform=str
    config.read(getFileDirPath(__file__) + '/config.ini')
    logger.info(f'config.ini loaded')

def read_config_file(filename):
    new_config = configparser.ConfigParser()
    new_config.read(filename)
    return new_config

def get_common_config(key):
    value = config.get(COMMON_CONFIG, key)
    return value

def get_os_config(key, default_val=None):
    return os.getenv(key, default_val)

def get_aws_config(key):
    return config.get(AWS_CONFIG, key)

def get_lake_config(env, key):
    return config.get(LAKE_CONFIG, env+'/'+key)

def get_sfcc_config(env, key):
    return config.get(SFCC_CONFIG, env+'/'+key)

def get_diva_config(env, key):
    return config.get(DIVA_CONFIG, env+'/'+key)

def get_diva_mapping(key):
    return config.get(DIVA_MAPPING, key)

def get_sfcc_attribute_mapping(key):
    return config.get(SFCC_ATTRIBUTE_MAPPING, key)

def get_config_section(section):
    return config[section]

def get_productsup_config(env, key):
    return config.get(PRODUCTSUP_CONFIG, env+'/'+key)

def get_productsup_mapping(key):
    return config.get(PRODUCTSUP_MAPPING, key)

def isTestModeEnabled():
    return get_os_config('TEST_MODE') == 'True'
