import logging
import sys

LEVEL = logging.INFO

# TODO - This method is to be removed once all lambda tests are shifted to pytest
def enableLocalLogs():
    # logging.basicConfig(level=LEVEL,
    #     format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
    #     datefmt='%m-%d %H:%M',
    #     # filename='./logs/lambda.log',
    #     # filemode='w'
    #     stream=sys.stdout
    # ) 
    return False

# This function is used to create a logger 
def getLogger(name):
    logger = logging.getLogger(name)
    logger.setLevel(LEVEL)

    # TODO - Any other logging config if required
    
    return logger
