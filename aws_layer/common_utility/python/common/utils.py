import requests
import json
import boto3
import os
import pysftp
import paramiko
import base64
import traceback

from common.config import PRODUCTSUP_CONFIG, PRODUCTSUP_MAPPING, config, get_aws_config, SFCC_ATTRIBUTE_MAPPING, LAKE_CONFIG, SFCC_CONFIG, DIVA_CONFIG, DIVA_MAPPING
from common.cxlLogger import getLogger

logger = getLogger(__name__)

# This function handles all REST API request 
def executeRequest(option, requestName):
    try: 
        logger.info(f'{requestName} - Request Start')
        logger.info(f'{requestName} - Request options - {option}')
        
        if option["method"] == 'GET':
            res = requests.get(option["url"], headers = option["headers"], data = option["inputBody"])
        elif option["method"]=='POST':
            res = requests.post(option["url"], headers = option["headers"], data = option["inputBody"])
            
        logger.info(f'{requestName} - Response Status - {str(res.status_code)}')
        logger.info(f'{requestName} - Response Body - {str(res.text)}')
        logger.info(f'{requestName} - Request End')
        
        return res
    except Exception as e:
        logger.error(f'Exception occurred while invoking API - {requestName}', exc_info = True)
        raise Exception(e)

# This function is used for creating ssm client 
def create_ssm_client():
    ssm = boto3.client(
        'ssm',
        region_name = get_aws_config('aws_region_name'),
        aws_access_key_id = get_aws_config('aws_access_key_id'),
        aws_secret_access_key = get_aws_config('aws_secret_access_key')
    )

    return ssm

# This function is used for getting values from parameter store based on string
def get_values_ssm(path):
    ssm = create_ssm_client()
    if path.startswith('/'):
        paginator = ssm.get_paginator('get_parameters_by_path')
        iterator = paginator.paginate(Path = path, WithDecryption = True, Recursive = True)
        params = []
        dict = {}
        for page in iterator:
            params.extend(page['Parameters'])
            for param in page.get('Parameters', []):
                dict[param.get('Name')] = param.get('Value')
        return dict
    else:
        response = ssm.get_parameter(Name = path)
        value = (response["Parameter"]["Value"])
        return value

# This function is used for creating sqs client
def create_sqs_client():
    sqs = boto3.client(
        'sqs',
        region_name = get_aws_config('aws_region_name'),
        aws_access_key_id = get_aws_config('aws_access_key_id'),
        aws_secret_access_key = get_aws_config('aws_secret_access_key')
    )

    return sqs

# This function is used for sending messages into sqs
def publish_sqs_message(queue_url, message_body, messageAttributes, delay=10):
    sqs = create_sqs_client()
    try:
        response = sqs.send_message(
            QueueUrl = queue_url,
            DelaySeconds = delay,
            MessageBody = json.dumps(message_body),
            MessageAttributes = messageAttributes
        )

        if(response["ResponseMetadata"]["HTTPStatusCode"] == 200):
            logger.info(f"Message is successfully delivered to SQS {queue_url} - Response : {response}")
        else:
            logger.info(f"Message is delivery failed to SQS {queue_url} - Response : {response}")

        return response
    except Exception as e:
        logger.error(f"Exception occurred while publishing message to {queue_url}", exc_info = True)
        raise Exception(e)

    return response

# This function is used to delete message in sqs
def delete_sqs_message(queue_url, receipt_handle):
    sqs = create_sqs_client()
    try:
        sqs.delete_message(
            QueueUrl=queue_url,
            ReceiptHandle=receipt_handle
        )
        logger.info('Message deleted')
    except Exception as e :
        logger.error("Message is not deleted")
        # raise Exception(e)

# This function is used to set values of lake config from parameter store inside config file 
def initialize_lake_config():
    logger.info('initialize_lake_config invoked')
    try:
        lake_config = get_values_ssm(f'/lake_config')
        for key, value in lake_config.items():
            new_key = key.replace('/lake_config/','')
            config.set(LAKE_CONFIG, new_key, value)
    except Exception as e:
        logger.error(f"Exception occured while retrieving lake_config from parameter store", exc_info = True)

# This function is used to set values of sfcc config from parameter store inside config file                 
def initialize_sfcc_config():
    logger.info('initialize_sfcc_config invoked')
    try:
        sfcc_config = get_values_ssm(f'/sfcc_config')
        for key, value in sfcc_config.items():
            new_key = key.replace('/sfcc_config/','')
            config.set(SFCC_CONFIG, new_key, value)
    except Exception as e:
        logger.error(f"Exception occured while retrieving sfcc_config from parameter store", exc_info = True)

# This function is used to set values of sfcc attribute mapping from parameter store inside config file                 
def initialize_sfcc_attribute_mapping():
    logger.info('initialize_sfcc_attribute_mapping invoked')
    try:
        # sfcc_attribute_mapping = get_values_ssm(f'/category-variant')
        sfcc_attribute_mapping = get_values_ssm(f'/sfcc_attribute_mapping')
        for key, value in sfcc_attribute_mapping.items():
            # new_key = key.replace('/category-variant/','')
            new_key = key.replace('/sfcc_attribute_mapping/','')
            config.set(SFCC_ATTRIBUTE_MAPPING, new_key, value)
    except Exception as e:
        logger.error(f"Exception occured while retrieving sfcc_attribute_mapping from parameter store", exc_info = True)
        # TODO - impl fall back logic if required
        raise Exception(e)

# This function is used to set values of diva config from parameter store inside config file
def initialize_diva_config():
    logger.info('initialize_diva_config invoked')
    try:
        diva_config = get_values_ssm(f'/diva_config')
        for key, value in diva_config.items():
            config.set(DIVA_CONFIG, key, value)
    except Exception as e:
        logger.error(f"Exception occured while retrieving diva_config from parameter store", exc_info = True)

# This function is used to set values of diva mappings from parameter store inside config file
def initialize_diva_mapping():
    logger.info('initialize_diva_mapping invoked')
    try:
        diva_mapping = get_values_ssm(f'/diva_mapping')
        for key, value in diva_mapping.items():
            config.set(DIVA_MAPPING, key, value)
    except Exception as e:
        logger.error(f"Exception occured while retrieving diva_mapping from parameter store", exc_info = True)

# This function is used to set values of productsup config from parameter store inside config file
def initialize_productsup_config():
    logger.info('initialize_productsup_config invoked')
    try:
        productsup_config = get_values_ssm(f'/productsup_config')
        for key, value in productsup_config.items():
            config.set(PRODUCTSUP_CONFIG, key, value)
    except Exception as e:
        logger.error(f"Exception occured while retrieving productsup_config from parameter store", exc_info = True)

# This function is used to set values of productsup mappings from parameter store inside config file
def initialize_productsup_mapping():
    logger.info('initialize_productsup_mapping invoked')
    try:
        productsup_mapping = get_values_ssm(f'/productsup_mapping')
        for key, value in productsup_mapping.items():
            config.set(PRODUCTSUP_MAPPING, key, value)
    except Exception as e:
        logger.error(f"Exception occured while retrieving productsup_mapping from parameter store", exc_info = True)

# This function is used for creating sqs client
def create_sftp_client(options):
    logger.info(f"create_sftp_client options : \n {options}")

    username = options['username']
    password = options['password']
    host = options['host']
    ssh_rsa = options['ssh_rsa']

    ssh_rsa_bytes = bytes(ssh_rsa, encoding='us-ascii')
    ssh_rsa_decoded = base64.decodebytes(ssh_rsa_bytes)
    key = paramiko.RSAKey(data = ssh_rsa_decoded)

    cnopts = pysftp.CnOpts()
    cnopts.hostkeys.add(host, 'ssh-rsa', key)
    sftp = pysftp.Connection(
        host, 
        username = username, 
        password = password, 
        cnopts = cnopts
    )
    return sftp

# This function is used for SFTP file upload
def sftp_upload(sftpClient, destinationPath, data):
    try:
        f = sftpClient.open(destinationPath, 'wb')
        f.write(data)
        logger.info(f"File upload successfully {destinationPath} using SFTP")
        logger.info(f"File data : \n {data}")
    except Exception as e:
        logger.error(f"Error occured while sending file {destinationPath} using SFTP", exc_info=True)
        raise Exception(e)

# This function is used for creating S3 generic client
def create_s3_generic_client(aws_access_key_id, aws_secret_access_key, aws_region_name = None):
    s3 = boto3.client(
        's3', 
        region_name = aws_region_name,
        aws_access_key_id = aws_access_key_id, 
        aws_secret_access_key = aws_secret_access_key
    )
    return s3

# This function is used for S3 upload
def s3_upload(s3Client, data, bucket, key):
    try:
        logger.info("S3 bucket {bucket}")
        logger.info("S3 bucket key {bucket}")
        logger.info("S3 upload data {data}")

        s3Client.put_object(Body=data, Bucket=bucket, Key=key)

        logger.info("Data uploaded successfully to bucket {bucket}")
    except Exception as e:
        logger.info(f"Error occured while uploading data to S3 bucket {bucket}", exc_info=True)
        raise Exception(e)

# def formatExceptionTrace():
#     return traceback.format_exc().replace('\n', '\n\t')

def delete_file_from_local(sourcePath):
    try:
        if os.path.exists(sourcePath):
            os.remove(sourcePath)
            logger.info(f"Removed the file {sourcePath} from local temporarily folder")
    except Exception as e:
        logger.info(f"Error while removing the file {sourcePath} from local temp folder", exc_info=True)
        raise Exception(e)