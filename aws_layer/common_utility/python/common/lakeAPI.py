import json
import base64
from datetime import datetime

from common.config import config, get_lake_config
from common.utils import executeRequest
from common.cxlLogger import getLogger

logger = getLogger(__name__)

def getProduct(gtin, env):
    try:
        username = get_lake_config(env, 'username')
        password = get_lake_config(env, 'password')
        inputBody = {
        }
        url = get_lake_config(env, 'product_url')
        auth = username + ":" + password
        encodedAuth = base64.b64encode(auth.encode()).decode()
        authHeader = "Basic " + encodedAuth

        headers = {
            "gtin": gtin,
            "timeStamp": str(datetime.now()),
            "Authorization": authHeader,
            "Content-Type": "application/json"
        }

        option = {}
        option["method"]='GET'
        option["url"]=url
        option["headers"]=headers
        option["inputBody"]=inputBody
        res = executeRequest(option, "Get Product")

        if res.status_code == 200:
            return json.loads(res.text)
        else:
            raise Exception(f'Get Product failed with response code {res.status_code}')

    except Exception as e:
        logger.error("Exception occurred while invoking Get Product API", exc_info = True)
        raise Exception(e)

def getArticle(gtin, env):
    try:
        username = get_lake_config(env, 'username')
        password = get_lake_config(env, 'password')
        inputBody = {
        }
        url = get_lake_config(env, 'article_url')
        auth = username + ":" + password
        encodedAuth = base64.b64encode(auth.encode()).decode()
        authHeader = "Basic " + encodedAuth
        
        headers = {
            "gtin": gtin,
            "timeStamp": str(datetime.now()),
            "Authorization": authHeader,
            "Content-Type": "application/json"
        }

        option = {}
        option["method"]='GET'
        option["url"]=url
        option["headers"]=headers
        option["inputBody"]=inputBody
        res = executeRequest(option, "Get Article")

        if res.status_code == 200:
            return json.loads(res.text)
        else:
            raise Exception(f'Get Article failed with response code {res.status_code}')
        
    except Exception as e:
        logger.error("Exception occurred while invoking Get Article API", exc_info=True)
        raise Exception(e)

def getMedia(gtin, env, mediaType = None, mediaId = None, resolutionKey=None, channel=None):
    try:
        username = get_lake_config(env, 'username')
        password = get_lake_config(env, 'password')
        inputBody = {
        }
        url = get_lake_config(env, 'media_url')
        auth = username + ":" + password
        encodedAuth = base64.b64encode(auth.encode()).decode()
        authHeader = "Basic " + encodedAuth
 
        headers = {
            "timeStamp": str(datetime.now()),
            "Authorization": authHeader,
            "Content-Type": "application/json",
            "gtin": gtin
        }
 
        if None != mediaType: headers['mediaType'] = mediaType
        if None != mediaId: headers['mediaId'] = mediaId
        if None != channel: headers['channel'] = channel
        if None != resolutionKey: headers['resolutionKey'] = resolutionKey
 
        option = {}
        option["method"]='GET'
        option["url"]=url
        option["headers"]=headers
        option["inputBody"]=inputBody
        res = executeRequest(option, "Get Media")
 
        if res.status_code == 200:
            return json.loads(res.text)
        else:
            raise Exception(f'Get Media failed with response code {res.status_code}')
 
    except Exception as e:
        logger.error("Exception occurred while invoking Get Media API", exc_info = True)
        raise Exception(e)

def getInventory(gtin, env):
    try:
        inputBody = {
        }
        url = get_lake_config(env, 'inventory_url') + '/' + gtin
        x_api_key = get_lake_config(env, 'best_x_api_key')
        headers = {
            "gtin": gtin,
            "timeStamp": str(datetime.now()),
            "x-api-xey": x_api_key,
            "Content-Type": "application/json"
        }

        option = {}
        option["method"]='GET'
        option["url"]=url
        option["headers"]=headers
        option["inputBody"]=inputBody
        res = executeRequest(option, "Get Inventory")

        if res.status_code == 200:
            return json.loads(res.text)
        else:
            raise Exception(f'Get Inventory failed with response code {res.status_code}')

    except Exception as e:
        logger.error("Exception occurred while invoking Get Inventory API", exc_info = True)
        raise Exception(e)
        