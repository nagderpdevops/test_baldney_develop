import base64
import json
from webdav3.client import Client

from common.config import get_sfcc_config
from common.utils import executeRequest
from common.cxlLogger import getLogger

logger = getLogger(__name__)

def uploadFileToWebDav(sourcePath, destinationPath, authToken, env):
    try:
        options = {'webdav_hostname': get_sfcc_config(env, "impex_url"), 'webdav_token' : authToken}
        logger.info(f'File upload to WebDav {destinationPath} - {options}')

        webdavClient = Client(options)
        directoryPath = destinationPath[:destinationPath.rfind("/")]
       
        if webdavClient.check(directoryPath) is False:
            webdavClient.mkdir(directoryPath)
            
        webdavClient.upload_sync(remote_path = destinationPath, local_path = sourcePath)

        logger.info(f'File upload successful - {destinationPath}')
    except Exception as e:
        logger.error(f"Exception occurred while uploading {destinationPath} to WebDav", exc_info = True)
        raise Exception(e)

def generateAuthToken(env):
    try:
        clientId = get_sfcc_config(env, 'client_id')
        clientSecret = get_sfcc_config(env, 'client_secret')
        url = get_sfcc_config(env, 'client_token_url')
        inputBody = { "grant_type": "client_credentials" }
        auth = clientId + ":" + clientSecret
        encodedAuth = base64.b64encode(auth.encode()).decode()
        authHeader = "Basic " + encodedAuth

        headers = {
            "Authorization": authHeader,
            "Content-Type": "application/x-www-form-urlencoded"
        }

        option = {}
        option["method"]='POST'
        option["url"]=url
        option["headers"]=headers
        option["inputBody"]=inputBody
        res = executeRequest(option, "Generate OCAPI auth token")

        if res.status_code == 200:
            return json.loads(res.text)
        else:
            raise Exception(f'Generate OCAPI auth token failed with response code {res.status_code}')

    except Exception as e:
        logger.error("Exception occurred while invoking Generate OCAPI auth token", exc_info = True)
        raise Exception(e)

