import base64
from common.cxlLogger import getLogger

logger = getLogger(__name__)

def prepareLambdaResponse(imageResponse):
    headers = { 
        "Content-Type": "image/jpeg" 
    }
    response = {
        'headers': headers,
        'statusCode': 200,
        'body': base64.b64encode(imageResponse.content).decode('utf-8'),
        'isBase64Encoded': True
    }
    return response

def get_fallback_image():
    converted_string = ''
    with open("assets/fallback.jpeg", "rb") as image2string:
        converted_string = base64.b64encode(image2string.read()).decode('utf-8')
    return converted_string

def prepare_fallback_response():
    headers = { 
        "Content-Type": "image/jpeg" 
    }
    response = {
        'headers': headers,
        'statusCode': 200,
        'body': get_fallback_image(),
        'isBase64Encoded': True
    }
    return response