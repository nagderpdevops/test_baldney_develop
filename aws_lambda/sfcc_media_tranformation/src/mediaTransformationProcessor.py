import requests
from common.lakeAPI import getMedia
from common.cxlLogger import getLogger
from src.mediaTransformationHelper import prepareLambdaResponse, prepare_fallback_response

logger = getLogger(__name__)

# TODO - move string litrals to constants
def process_media_transformation(gtin, mediaType, mediaId, channel, env):
    try:

        if not channel: channel = "gkkDigitalDataManagement"

        mediaResponse = getMedia(gtin, env, mediaType, mediaId, "ORIGINAL", channel)
        sourceURL = mediaResponse['mediaAssets'][0]['sourceUrl']

        option = {}
        option["method"] = 'GET'
        option["url"] = sourceURL
        option["headers"] = {}
        option["inputBody"] = {}

        imageResponse = requests.get(sourceURL)

        return prepareLambdaResponse(imageResponse)
    except Exception as e:
        logger.error(f"Exception occurred while processing media transformation request", exc_info=True)
        raise Exception(e)

def processEvent(event):
    try:
        env = event['queryStringParameters']['env']
        gtin = event['queryStringParameters']['gtin']
        mediaType = event['queryStringParameters'].get('mediaType')
        mediaId = event['queryStringParameters'].get('mediaId')
        channel = event['queryStringParameters'].get('channel')

        logger.info(f'env - {env}')
        logger.info(f'gtin - {gtin}')
        logger.info(f'mediaType - {mediaType}')
        logger.info(f'mediaId - {mediaId}')
        logger.info(f'channel - {channel}')

        response = process_media_transformation(gtin, mediaType, mediaId, channel, env)
        return response
    except Exception as e:
        logger.error("Exception occurred in processEvent", exc_info=True)
        return prepare_fallback_response()