import testConfig
from testHelper import readInputEventFile, writeOutputImageFile
from handler import lambda_handler

# Local test function with no mocks
def test_local():
    event = readInputEventFile('localTestEvent.json')
    response = lambda_handler(event, {})
    writeOutputImageFile(response['body'], "test_success_image.jpeg")