import json
import base64

def readInputEventFile(fileName):
    with open(f'test/input/{fileName}', 'r') as read_file:
        inputData = json.load(read_file)
    return inputData

def writeOutputImageFile(imgData, fileName):
    with open(f"test/output/{fileName}", "wb") as fh:
        fh.write(base64.b64decode(imgData))

def readMockImageFile(fileName):
    with open(f"test/mock/{fileName}", "rb") as image2string:
        return image2string.read()

def convertBytesToString(bytes):
    return base64.b64encode(bytes).decode('utf-8')

def newObject(name, data):
    return type(name, (object,), data)
