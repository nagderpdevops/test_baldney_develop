import testConfig
from testHelper import readInputEventFile, writeOutputImageFile, newObject, readMockImageFile, convertBytesToString
from handler import lambda_handler

def test_success_image(mocker):
    imageContent = readMockImageFile('pink_socks.jpeg')
    # Mocks
    mocker.patch('handler.initialize_lake_config')
    mocker.patch('src.mediaTransformationProcessor.getMedia', return_value={ "mediaAssets": [{ "sourceUrl": "DUMMY_URL" }] })
    mocker.patch('src.mediaTransformationProcessor.requests.get', return_value=newObject('ImageResponse', { "content": imageContent }))
    
    event = readInputEventFile('mainImageEvent.json')
    response = lambda_handler(event, {})
    statusCode = response['statusCode']
    
    assert statusCode == 200, "Lambda response status code was not 200"
    assert response['body'] == convertBytesToString(imageContent)
    
def test_fallback_image(mocker):
    imageContent = readMockImageFile('fallback.jpeg')
    # Mocks
    mocker.patch('handler.initialize_lake_config')
    mocker.patch('src.mediaTransformationProcessor.getMedia', return_value={ "mediaAssets": [] })

    event = readInputEventFile('fallbackImageEvent.json')
    response = lambda_handler(event, {})
    statusCode = response['statusCode']

    assert statusCode == 200, "Lambda response status code was not 200"
    assert response['body'] == convertBytesToString(imageContent)
