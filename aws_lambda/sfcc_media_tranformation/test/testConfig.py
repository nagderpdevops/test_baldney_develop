from os import sys, path
COMMON_PATH = path.dirname(path.dirname(path.dirname(path.dirname(path.abspath(__file__))))) + '/aws_layer/common_utility/python'
ROOT_PATH = path.dirname(path.dirname(path.abspath(__file__)))
sys.path.append(COMMON_PATH)
sys.path.append(ROOT_PATH)