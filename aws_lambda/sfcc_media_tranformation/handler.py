from src.mediaTransformationProcessor import processEvent
from src.mediaTransformationHelper import prepare_fallback_response
from common.cxlLogger import getLogger
from common.config import initialize, config, LAKE_CONFIG
from common.utils import initialize_lake_config

logger = getLogger(__name__)

def cache_section_config(section, set_config_callback):
    try:
        is_cached = config.get(section, 'is_cached')
        logger.info(f'{section} is_cached already set to - ' + is_cached)
    except Exception as e:
        logger.info(f'{section} is not cached')
        set_config_callback()
        config.set(section, 'is_cached', 'true')
        logger.info(f'{section} is_cached is now set to true')

def lambda_handler(event, context):
    try:
        logger.info(f'Event - {str(event)}')

        initialize()
        cache_section_config(LAKE_CONFIG, initialize_lake_config)

        response = processEvent(event)
        logger.info(f'Lambda response - {response}')
        return response
    except Exception as e:
        logger.error("Exception occurred lambda_handler", exc_info=True)
        return prepare_fallback_response()